import React from 'react'
import './Home.css'
import Banner from '../../components/Banner/Banner'
import MainNav from '../../components/MainNavbar/MainNav'
import Header from '../../components/Header/Header'
import Footer from '../../components/Footer/Footer'
const Home = () => {
  return (
    <div>
      <MainNav/>
      <Banner/>
      <Header/>
      <Footer/>
    </div>
  )
}

export default Home