import React, { Fragment } from 'react'
import './Contact.css'
import MainNav from '../../components/MainNavbar/MainNav'
import Banner from '../../components/Banner/Banner'
import Footer from '../../components/Footer/Footer'

import {AiOutlineArrowRight} from 'react-icons/ai'

import contact1 from '../../assets/images/contact/contact-1.jpg'
import contact2 from '../../assets/images/contact/contact-2.jpg'
import contact3 from '../../assets/images/contact/contact-3.jpg'
import contact4 from '../../assets/images/contact/contact-4.jpg'
import contact5 from '../../assets/images/contact/contact-5.jpg'
import contact6 from '../../assets/images/contact/contact-6.jpg'
const Contact = () => {
  return (
    <Fragment>
      <MainNav/>
      <Banner/>
      <div className="container contact__container">
        <div className="contact__left">
          <div className="contact__left-gallery">
            <img src={contact1} alt="" />
            <img src={contact2} alt="" />
          </div>
          <div className="contact__left-gallery">
            <img src={contact3} alt="" />
            <img src={contact4} alt="" />
          </div>
          <div className="contact__left-gallery">
            <img src={contact5} alt="" />
            <img src={contact6} alt="" />
          </div>
        </div>
        <div className="contact__right">
          <h2>Contact Us</h2>
          <div className="contact__right-wrapper">
              <div className="contact__right-form">
                <label>Full Name</label>
                <input type="text" placeholder='Your Name...'/>
              </div>

              <div className="contact__right-form">
                <label>Email Address</label>
                <input type="text" placeholder='Your Email...'/>
              </div>

              <div className="contact__right-form">
                <label>Reason</label>
                <input type="text" placeholder='Your Reason...'/>
              </div>

              <div className="contact__right-form">
                <label>Message</label>
                <textarea rows={10}></textarea>
              </div>
              <div className="contact__right-form">
                <button>Send Message <AiOutlineArrowRight className='contact__right-form__icon'/></button>
              </div>
          </div>
        </div>
      </div>
      <Footer/>
      
    </Fragment>
  )
}

export default Contact