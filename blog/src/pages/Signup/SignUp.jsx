import React, { useState, useReducer } from "react";
import "./SignUp.css";
import Topbar from "../../components/Topbar/Topbar";
import {
  AiOutlineMail,
  AiOutlineLock,
  AiOutlineUser,
  AiFillCheckCircle,
  AiOutlineEye,
  AiOutlineEyeInvisible,
} from "react-icons/ai";
import { Link } from "react-router-dom";
import Modal from "../../components/Modal/Modal";
import { initialState, modalReducer } from "../../Reducer/ModalReducer";
const SignUp = () => {
  //toggle password
  const [togglePass, setTogglePass] = useState(false);
  //toggle confirm password
  const [toggleConfirmPass, setToggleConfirmPass] = useState(false);
  //password content
  const [password, setPassword] = useState("");
  //confirm password content
  const [confirmPassword, setConfirmPassword] = useState("");
  // Modal Reducer
  const [state, dispatch] = useReducer(modalReducer, initialState)
  //HANDLE PASSWORD ONCLICK EVENT
  const handlePassword = (e, type) => {
    e.preventDefault();
    if (type == "confirm") {
      setTogglePass(!togglePass);
    } else if (type == "second-confirm") {
      setToggleConfirmPass(!toggleConfirmPass);
    }
  };

  //HANDLE SIGN UP ONCLICK EVENT
  const handleSignUpClick = (e) => {
    e.preventDefault();
    if (password !== confirmPassword) {
      
      dispatch({type: "TOGGLE_MODAL", payload: {title:"Alert Modal",content: "Your password is invalid"}})
     
    }
  };
  
  return (
    <div className="signup-container">
      <Topbar />
      <div className="container">
        {/* PAGE TITLE */}
        <div className="signup-title">
          <h2>SIGN UP</h2>
        </div>
        {/*END OF PAGE TITLE */}

        {/* SIGN UP FORM */}
        <div className="signup-form">
          <div className="form-item">
            <label>
              <AiOutlineUser className="form-item-icon" /> Username
            </label>
            <div className="form-item-input">
              <input type="text" placeholder="Username" />
              <span>Check</span>
            </div>
          </div>
          <div className="form-item">
            <label>
              <AiOutlineMail className="form-item-icon" /> Email
            </label>
            <div className="form-item-input">
              <input type="email" placeholder="Email" />
              <span>Check</span>
            </div>
          </div>
          <div className="form-item">
            <label>
              <AiOutlineLock className="form-item-icon" /> Password
            </label>
            <div className="form-item-input">
              <input
                type={togglePass ? "text" : "password"}
                placeholder="Password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <span
                className="first"
                onClick={(e) => handlePassword(e, "confirm")}
              >
                {togglePass ? <AiOutlineEyeInvisible /> : <AiOutlineEye />}
              </span>
            </div>
          </div>
          <div className="form-item">
            <label>
              <AiOutlineLock className="form-item-icon" />
              Confirm Password
            </label>
            <div className="form-item-input">
              <input
                type={toggleConfirmPass ? "text" : "password"}
                placeholder="Confirm password"
                onChange={(e) => setConfirmPassword(e.target.value)}
              />
              <span
                className="second"
                onClick={(e) => handlePassword(e, "second-confirm")}
              >
                {toggleConfirmPass ? (
                  <AiOutlineEyeInvisible />
                ) : (
                  <AiOutlineEye />
                )}
              </span>
            </div>
          </div>
          <div className="form-item">
            <button className="btn" onClick={(e) => handleSignUpClick(e)}>
              Sign Up
            </button>
          </div>
          <div className="form-item">
            <p>
              Already have account?
              <Link to="/login">
                <a href="#">Sign In</a>
              </Link>
            </p>
          </div>
        </div>
        {/*END OF SIGN UP FORM */}
      </div>
      <Modal  state={state} dispatch={dispatch}/>
    </div>
  );
};

export default SignUp;
