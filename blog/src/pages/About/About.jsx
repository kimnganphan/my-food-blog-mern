import React from "react";
import "./About.css";
//import components
import MainNav from "../../components/MainNavbar/MainNav";
import Banner from "../../components/Banner/Banner";
import Footer from "../../components/Footer/Footer";
//import images
import AboutBackground from "../../assets/images/about/about-4.jpg";
import AboutHeaderImg from "../../assets/images/about/cupcake-1.avif";
import AboutQuoteImg from "../../assets/images/about/about-6.avif";
import Recipe1 from "../../assets/images/about/about-1.jpg";
import Recipe2 from "../../assets/images/about/about-2.jpg";
import Recipe3 from "../../assets/images/about/about-5.avif";

import { Link } from "react-router-dom";
import { AiFillHeart } from "react-icons/ai";
import { reviewers } from "../../Data";

const About = () => {
  return (
    <div className="about">
      <MainNav />
      <Banner />

      <div className="about-header">
        <div className="about-header-background">
          <img src={AboutBackground} alt="main-about-background" />
        </div>

        <div className="about-img">
          <img src={AboutHeaderImg} alt="" />
        </div>
        <div className="about-header-desc">
          <div className="title">
            Hello! My name is Cheryl. I'm founder of Cheryl Food Blog
          </div>
          <div className="desc">
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam
            autem magni et voluptatibus adipisci earum pariatur sunt illo modi
            laborum vero, quibusdam sit eum quam odit nihil, aut dolores ab?
          </div>
          <Link to="/contact">
            <button className="btn">Contact Me</button>
          </Link>
        </div>
      </div>
      <div className="container">
        <div className="about-header-mobile">
          <div className="about-header-mobile-wrapper">
            <div className="about-mobile-img">
              <img src={AboutHeaderImg} alt="" />
            </div>
            <div className="about-mobile-desc">
              <div className="title">
                Hello! My name is Cheryl. I'm founder of Cheryl Food Blog
              </div>
              <div className="desc">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi
                illum minus, consectetur consequuntur voluptatibus consequatur
                ea laboriosam! Dolorem, repellat facere perspiciatis quasi magni
                cupiditate nostrum porro doloremque, quas vel molestias.
              </div>
              <Link to="/contact">
                <button className="btn">Contact Me</button>
              </Link>
            </div>
          </div>
        </div>
        <div className="about-quote">
          <div className="quote">
            <h2>Our Motivation</h2>" It's always great to engage with people.
            You never know who you can make an effect on. And I love interacting
            with the fans, hearing what they have to say and joking around with
            them. Anytime I can reach out online and give encouragement,
            motivate people, be a better citizen, that's what it's all about,
            man. "
          </div>
          <div className="quote-img">
            <img src={AboutQuoteImg} alt="" />
          </div>
        </div>

        <div className="about-recipe">
          <div className="recipe-img">
            <div className="recipe-img-row">
              <img src={Recipe1} alt="recipe 1" />
              <img src={Recipe2} alt="recipe 2" />
            </div>
            <div className="recipe-img-row">
              <img src={Recipe3} alt="recipe 3" className="recipe-img-3" />
            </div>
          </div>
          <div className="recipe-desc">
            <h2>Our Recipes</h2>
            Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam
            autem magni et voluptatibus adipisci earum pariatur sunt illo modi
            laborum vero, quibusdam sit eum quam odit nihil, aut dolores ab?
          </div>
        </div>

        <div className="about-review">
          <h2>
            <AiFillHeart className="about-review-icon" /> Our Beloved Blogger
            Reviews <AiFillHeart className="about-review-icon" />
          </h2>
          <div className="about-review-lists">
            {reviewers.map((item) => {
              return (
                <div className="about-review-listItem" key={item.id}>
                  <div className="reviewer-img">
                    <img
                      src={require(`../../assets/images/about/${item.img}`)}
                      alt="reviewer"
                    />
                  </div>
                  <div className="reviewer-content">
                    <h3>{item.name}</h3>
                    <p>{item.content}</p>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>

      <Footer />
    </div>
  );
};

export default About;
