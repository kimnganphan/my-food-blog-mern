import React, { useState } from "react";
import "./Login.css";
import Topbar from "../../components/Topbar/Topbar";
import cupcakePic from "../../assets/images/login/login-2.jpg";
import {
  AiOutlineMail,
  AiOutlineLock,
  AiOutlineUser,
  AiFillCheckCircle,
  AiOutlineEye,
  AiOutlineEyeInvisible,
  
} from "react-icons/ai";
import {BsDashLg} from 'react-icons/bs'
import { Link } from "react-router-dom";

const Login = () => {
  //toggle password
  const [togglePass, setTogglePass] = useState(false);
  return (
    <div className="login-container">
      <Topbar />
      <div className="container">
        {/* PAGE TITLE */}
        <div className="login-title">
          <p>Welcome</p>
          <h2>SIGN IN</h2>
          <p>And enjoy writing your own blog</p>
        </div>
        {/* END OF PAGE TITLE */}
      </div>

      {/* LOGIN FORM */}
      <div className="login-content">
        {/* LOGIN FORM LEFT PIC */}
        <div className="login-content-left">
          <img src={cupcakePic} alt="login pic" />
        </div>
        {/*END OF LOGIN FORM LEFT PIC */}
        {/* LOGIN FORM RIGHT */}
        <div className="login-content-right">
          <div className="login-form-container">
            <div className="login-form-item">
              <label>
                <AiOutlineUser />
                Username
              </label>
              <div className="login-inp-container">
                <input type="text" placeholder="Username" />
                <AiFillCheckCircle className="login-inp-container-icon"/>
              </div>
            </div>

            <div className="login-form-item">
              <label>
                <AiOutlineLock />
                Password
              </label>
              <div className="login-inp-container">
                <input type={togglePass == true ? "text" : "password"} placeholder="Password" />
                <AiOutlineEye className="login-inp-container-icon" onClick={e=>setTogglePass(!togglePass)}/>
              </div>
            </div>

            <div className="login-form-item">
              <button className="btn btn-lightblue">Sign In</button>
            </div>

            <p> Or </p>
            <div className="login-form-item">
              <button className="btn btn-red">Sign In With Google</button>
            </div>
            
            <p>If you don't have account. <Link to="/signup"><a href="#">Sign Up Here !</a></Link> </p>
          </div>
        </div>
      </div>
      {/* END OF LOGIN FORM */}
    </div>
  );
};

export default Login;
