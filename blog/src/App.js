//IMPORT PAGES
import Home from './pages/Home/Home'
import Login from './pages/Login/Login'
import SignUp from './pages/Signup/SignUp';
import Contact from './pages/Contact/Contact'
import About from './pages/About/About'
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <Routes>
      <Route exact path="/" element={<Home />}></Route>
      <Route  path="/login" element={<Login />}></Route>
      <Route  path="/signup" element={<SignUp />}></Route>
      <Route  path="/contact" element={<Contact />}></Route>
      <Route  path="/about" element={<About />}></Route>
    </Routes>
  );
}

export default App;
