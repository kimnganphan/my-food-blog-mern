import React from 'react'
import './MainNav.css'
import {Link} from 'react-router-dom'
const MainNav = () => {
  return (
    <nav className='navbar__wrapper'>
      {/* LOGO */}
      <div className="nav__logo">Cheryl.</div>
      {/*END OF LOGO */}
      <div className="nav__container">
        <div className="nav__container-lists">
          <Link to="/">
          <a href="#">Home</a>
          </Link>
          
          <Link to="/about">
          <a href="#">About Us</a>
          </Link>
          <Link to="/contact">
          <a href="#">Contact</a>
          </Link>
          <Link to="/">
          <a href="#">Write</a>
          </Link>
          <Link to="/">
          <a href="#">Posts</a>
          </Link>
        </div>
      </div>
      <div className="nav__right">
        <a href="#">Sign In</a>
      </div>
    </nav>
  )
}

export default MainNav