import React from "react";
import "./Modal.css";
import { AiOutlineClose } from "react-icons/ai";
import alertPic from '../../assets/images/modal/alert-1.jpg'
const Modal = (props) => {
  const handleCloseModal = (e, type) => {
    e.stopPropagation();
    if (type === "modal") {
      props.dispatch({
        type: "TOGGLE_MODAL",
        payload: { title: "", content: "" },
      });
    } else if (type == "icon") {
      props.dispatch({
        type: "TOGGLE_MODAL",
        payload: { title: "", content: "" },
      });
    }
    
  };

  return (
    <div
      className={`modal ${props.state.isDisplay == true ? "active" : " "}`}
      onClick={(e) => handleCloseModal(e, "modal")}
    >
      <div className="modal-content">
        <div className="modal-top">
          <div className="modal-title">{props.state.modalTitle}</div>
          <div className="modal-icon">
            <AiOutlineClose onClick={(e) => handleCloseModal(e, "icon")} />
          </div>
        </div>

        <div className="modal-bottom">
          <div className="modal-bottom-left">
              <img src={alertPic} alt="" />
              <div className="modal-bottom-message">
                  Please check your info again!
              </div>
          </div>
          <div className="modal-bottom-right">{props.state.modalContent}</div>
        </div>
      </div>
    </div>
  );
};

export default Modal;
