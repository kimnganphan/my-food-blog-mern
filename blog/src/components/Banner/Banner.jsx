import React from "react";
import "./Banner.css";
import { AiFillHeart, AiOutlineArrowRight } from "react-icons/ai";
import { Link } from "react-router-dom";
const Banner = () => {
  return (
    <section className="banner">
      <AiFillHeart className="banner__heart-icon" /> Write your own blog.{" "}
      <Link to="/signup">
        <a href="#">
          Sign Up <AiOutlineArrowRight className="banner__arrow-icon" />
        </a>
      </Link>
    </section>
  );
};

export default Banner;
