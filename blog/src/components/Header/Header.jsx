import React from "react";
import "./Header.css";
import Header1 from "../../assets/images/header/header-1.jpg";
import Header2 from "../../assets/images/header/header-2.jpg";
import Header3 from "../../assets/images/header/header-3.jpg";
import Header4 from "../../assets/images/header/header-4.jpg";
import Header5 from "../../assets/images/header/header-5.jpg";
const Header = () => {
  return (
    <header>
      <div className="img-container">
        <div className="img-container-left">
          <img src={Header2} alt="" />

          <img src={Header3} alt="" />
        </div>
        <div className="img-container-center">
          <img src={Header1} alt="" />
        </div>
        <div className="img-container-right">
          

          <img src={Header5} alt="" />
          <img src={Header4} alt="" />
        </div>
      </div>
    </header>
  );
};

export default Header;
