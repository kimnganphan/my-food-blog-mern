import React, { useEffect, useState } from "react";
import "./Topbar.css";
import { Link, useLocation } from "react-router-dom";
import { MdDashboard } from "react-icons/md";
import { AiOutlineClose } from "react-icons/ai";
const Topbar = () => {
  const [buttonContent, setButtonContent] = useState("");
  const [page, setPage] = useState("");
  const [toggleDashboard, setToggleDashboard] = useState(false);
  const location = useLocation();
  useEffect(() => {
    if (location.pathname == "/signup") {
      setButtonContent("Sign In");
      setPage("/login");
    } else if (location.pathname == "/login") {
      setButtonContent("Sign Up");
      setPage("/signup");
    }
  }, [location]);

  //HANDLE DASHBOARD ONCLICK
  const handleDashboardClick = (e) => {
    e.preventDefault();
    setToggleDashboard(true);
  };
  const handleCloseDashboard = (e, type) => {
    e.preventDefault();
    if (type === "icon" || type === "background") {
      setToggleDashboard(false);
    }
  };
  return (
    <nav className="topbar">
      <div
        className="topbar-dashboard"
        onClick={(e) => handleDashboardClick(e)}
      >
        <MdDashboard />
      </div>
      <Link to="/">
        <div className="topbar-logo">
          Cheryl<span>.</span>
        </div>
      </Link>
      <div
        className={`topbar-list-container ${
          toggleDashboard == true ? "active" : ""
        }`}
        onClick={(e) => handleCloseDashboard(e, "background")}
      >
        <div className="topbar-lists">
          <a
            href="#"
            className="topbar-lists-close-icon"
            onClick={(e) => handleCloseDashboard(e, "icon")}
          >
            <AiOutlineClose />
          </a>

          <Link to="/">
            <a href="topbar-listItem">Home</a>
          </Link>
          <Link to="/about">
            <a href="topbar-listItem">About Us</a>
          </Link>

          <Link to="/contact">
            <a href="topbar-listItem">Contact</a>
          </Link>
        </div>
      </div>

      <div className="topbar-button">
        <Link to={page}>
          <button className="btn">{buttonContent}</button>
        </Link>
      </div>
    </nav>
  );
};

export default Topbar;
