import React from "react";
import "./Footer.css";
import { AiOutlineInstagram, AiOutlineTwitter } from "react-icons/ai";
import { BsFacebook } from "react-icons/bs";
import { Link } from "react-router-dom";
const Footer = () => {
  return (
    <footer>
      <div className="container">
        <div className="footer__wrapper">
          {/* FOOTER LEFT */}
          <div className="footer__left">
            {/* SERVICES */}
            <div className="footer__left-item">
              <h3>Services</h3>
              <div className="footer__left-item-lists">
                <Link to="/">
                  <a href="#">Home</a>
                </Link>
                <Link to="/about">
                  <a href="#">About Us</a>
                </Link>
                <Link to="/contact">
                  <a href="#">Contact</a>
                </Link>

                <a href="#">Posts</a>
                <a href="#">Write blog</a>
                <a href="#">Privacy & Terms</a>
              </div>
            </div>
            {/* END OF SERVICES */}

            {/* REVIEWS AND RECIPE */}
            <div className="footer__left-item">
              <h3>Reviews & Recipes</h3>
              <div className="footer__left-item-lists">
                <a href="#">Reviews</a>
                <a href="#">Vietnamese Recipe</a>
                <a href="#">Western Recipe</a>
                <a href="#">Vegan Recipe</a>
                <a href="#">Chinese Recipe</a>
                <a href="#">Desserts</a>
              </div>
            </div>
            {/* END OF REVIEWS AND RECIPE */}
          </div>
          {/* END OF FOOTER LEFT */}

          {/* FOOTER RIGHT */}
          <div className="footer__right">
            {/* LOGO */}
            <div className="footer__right-logo">Cheryl.</div>
            {/* END OF LOGO */}

            {/* ICON */}

            <div className="footer__right-socials">
              <h4>Follow Us</h4>
              <div className="footer__right-socials-icon">
                <a href="#">
                  <AiOutlineInstagram />
                </a>
                <a href="#">
                  <AiOutlineTwitter />
                </a>
                <a href="#">
                  <BsFacebook />
                </a>
              </div>
            </div>
            {/* END OF ICON */}
          </div>
          {/* END OF FOOTER RIGHT */}
        </div>
      </div>
    </footer>
  );
};

export default Footer;
