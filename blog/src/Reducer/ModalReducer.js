export const initialState = {
    isDisplay: false,
    modalTitle : "",
    modalContent: ""
}

export const modalReducer = (state, action) => {
    switch (action.type) {
        case "TOGGLE_MODAL":
            
           return {
               isDisplay: !state.isDisplay,
               modalTitle: action.payload.title,
               modalContent: action.payload.content
           }
    
        default:
            return state;
    }
}