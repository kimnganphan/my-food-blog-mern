export const reviewers = [
    {
        id:1,
        content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.",
        img: "about-review-1.jpg",
        name: "James Franco"
    },
    {
        id:2,
        content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.",
        img: "about-review-2.jpg",
        name: "Harry Styles"
    },
    {
        id:3,
        content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.",
        img: "about-review-3.jpg",
        name: "Taylor Swift"
    },
    {
        id:4,
        content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.",
        img: "about-review-4.jpg",
        name: "Selena Gomez"
    },
    {
        id:5,
        content: "Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.        Lorem ipsum dolor sit amet consectetur, adipisicing elit. Doloribus quod totam ut neque delectus? Eius alias laudantium repellat ducimus dicta amet a sed error magnam. Aliquam id iure ratione repellat.",
        img: "about-review-5.jpg",
        name: "Jonny Depp"
    },
]